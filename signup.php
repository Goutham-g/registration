<?php
include("conn.php");
global $connection;
if( isset($_POST['insertButton']))
{
  $firstName = $_POST['firstName'];
  $lastName =  $_POST['lastName'];
  $email =  $_POST['email'];
  $dob =  $_POST['dob'];
  $password =  $_POST['password'];
  $confirmPassword =  $_POST['confirmPassword'];
  // var_dump($_FILES['myfile']);

  $filename = $_FILES['myfile']['name'];
    $temp = $_FILES['myfile']['tmp_name'];
    $new_file_name = time().$filename;
    move_uploaded_file($temp,"uploads/".$new_file_name);
    
 
  if($password == $confirmPassword)
  {
    
  
    $insert_query = "INSERT INTO reg (firstname,lastname,email,dob,password,confirmpassword,image)VALUES ('$firstName','$lastName', '$email','$dob','$password','$confirmPassword','$new_file_name')"; 
    $result = mysqli_query($connection,$insert_query);
    if($result==true)
    {
      ?><script> alert("Done");</script><?php
    }
  }
  else
  {
    ?><script>alert("password missmatch");</script><?php
  }
  
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js" integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>    
    <link rel="stylesheet" type="text/css" href="style.css">
    <title>SignUp</title>
<style>
#mainDiv
{
  margin-top: 2em;
  display: flex;
  justify-content: center;
}
</style>
</head>
<body>

    <h1 class="text-center"><u>Registration Here</u></h1>
    <div class="container" id="mainDiv">
      
        <form method="post" enctype="multipart/form-data">
            <div class="mb-3" >
            <label for="nameoftheuser" class="form-label">First Name</label>
            <input type="text" class="form-label" name="firstName" id="nameoftheuser" required>
         </div>
    
         <div class="mb-3" >
            <label for="lastnameoftheuser" class="form-label">Last Name</label>
            <input type="text" class="form-label" name="lastName" id="lastnameoftheuser" required>
         </div>

        
        <div class="mb-3">
            <label for="email" class="form-label">Email address</label>
            <input type="email" class="form-label" name="email" id="email"  pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$" title="Fill in correct id" required>
            <span id="msg" style="position:absolute"></span>
          </div>

          

         <div class="mb-3" >
            <label for="dob" class="form-label">DOB</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <input type="date" class="form-label" name="dob" id="doboftheuser" required>
         </div>



              
            <div  class="mb-3">
              <label for="exampleInputPassword1" class="form-label">Password</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
              <input type="password" name="password" pattern=".{6,}" title="Enter six or more charecter"  id="exampleInputPassword1" required
               type="password" class="form-control" name="password" value="secret">
            
            </div>
           
            
            
            <div  class="mb-3">
              <label for="exampleInputPassword2" class="form-label">Confirm Password</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
              <input type="password" name="confirmPassword" id="exampleInputPassword2" required>
            </div>
            <div  class="mb-3">
              <label for="file" class="form-label">Upload File</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
              <input type="file" name="myfile" id="fileupload" >
            </div>
            <button type="submit" name="insertButton" class="btn btn-secondary">Submit</button>
            Already have an account? <a href="login.php" style="color:blue;">login Here</a>
          </form>
    </div>

     
    
</body>
</html>

<script type="text/javascript"> 
 $(document).ready(function()
 {  
      $('#email') .keyup(function(){  
           var email = $('#email').val();  
           if(email== "")
          {
                $("#msg").fadeOut();
           }
           else
           {
                $.ajax({
                          url: "checkemail.php",
                          method: "POST",
                          data:{email:email},
                          success: function(data)
                          {
                               $("#msg").fadeIn().html(data);
                          }
                     });
           }

     }); 
             
 });  



            
 </script> 